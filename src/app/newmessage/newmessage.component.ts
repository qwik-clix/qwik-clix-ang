import { Component } from '@angular/core';
import { WebService } from '../web.services/web.service'

@Component({
  selector: 'new-message',
  templateUrl: './newmessage.component.html',
  styleUrls: ['./newmessage.component.css']
})
export class NewMessagesComponent {
  constructor(private webService: WebService) {

  }

  // LOCAL VARIABLES
  message = {
    userId: 1,
    userName: 'bill',
    text: "",
    timestamp:""
  }

  // METHODS
  post() {
    this.webService.postMessage(this.message)
  }
} 
