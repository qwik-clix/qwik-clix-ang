import { Component } from '@angular/core';
import { MessagesComponent } from '../messages/messages.component';
import { NewMessagesComponent } from '../newmessage/newmessage.component';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  constructor() { }

  ngOnInit(): void {
  }

}
