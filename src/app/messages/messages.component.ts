import { Component } from '@angular/core'
import { WebService } from '../web.services/web.service'
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'messages',
    styleUrls: ['./messages.component.css'],
    templateUrl: './messages.component.html'
})
export class MessagesComponent {

    constructor(public webService: WebService, private route: ActivatedRoute) {

    }

    ngOnInit() {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        const currentRoute = this.route.snapshot.params.name;
        this.webService.getMessages(currentRoute)
        
    };

} 