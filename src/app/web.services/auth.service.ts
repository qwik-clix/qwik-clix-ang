import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { IWrapperResponse, IUser, ILoginData } from '../utilities/interfaces';
import { BASE_URL } from '../utilities/urls';
import { Helper } from '../utilities/helpers';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
@Injectable()
export class AuthService {

    constructor(private http: HttpClient, private snackBar: MatSnackBar, private router: Router) {
    }

    private helper = new Helper(this.snackBar);
    LOGINUSER_KEY = 'loginUser';

    get loggedInUser() {
        return JSON.parse(localStorage.getItem(this.LOGINUSER_KEY));
    }

    get isAuthenticated() {
        return !!localStorage.getItem(this.LOGINUSER_KEY);
    }

    get tokenHeader() {
        var header = new HttpHeaders({ 'Authorization': 'Bearer ' + this.loggedInUser.token });
        return { headers: header }
    }

    register(user: IUser) {
        delete user.confirmPassword;

        this.http.post<IWrapperResponse>(`${BASE_URL}/auth/registerjwt`, user)
            .subscribe(
                response => {
                    this.authenticate(response);
                },
                error => {
                    this.helper.handleRegisterError(error, 'register');
                });
    }

    login(loginUser: ILoginData) {
        this.http.post<IWrapperResponse>(`${BASE_URL}/auth/login`, loginUser)
            .subscribe(
                response => {
                    this.authenticate(response);
                },
                error => {
                    this.helper.handleRegisterError(error, 'register');
                });
    }

    logout() {
        localStorage.removeItem(this.LOGINUSER_KEY);
    }

    authenticate(res: IWrapperResponse) {
        const { result: authResponse }: any = res;
        if (!authResponse.token) {
            return;
        }
        localStorage.setItem(this.LOGINUSER_KEY, JSON.stringify(authResponse));
        this.router.navigate(['/']);
    }

}