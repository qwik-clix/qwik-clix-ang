import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { IMessage, IWrapperResponse } from '../utilities/interfaces';
import { BASE_URL } from '../utilities/urls';
import { Helper } from '../utilities/helpers';
import { AuthService } from './../web.services/auth.service'

@Injectable()
export class WebService {

    constructor(private http: HttpClient, private snackBar: MatSnackBar, private route: ActivatedRoute, private auth: AuthService) {
        const currentRoute = this.route.snapshot.params.name;
        this.getMessages(currentRoute);
        this.getMeUser();
    }

    private helper = new Helper(this.snackBar);
    private messageStore: IMessage[] = [];
    private messageSubject = new Subject();
    messages = this.messageSubject.asObservable();

    getMessages(user: any) {

        // const userId = user.userId ? `${user.userId}/` : '';

        this.http.get<IWrapperResponse>(`${BASE_URL}/messages/1/get`)
            .subscribe(
                response => {
                    const { messages }: any = response.result;
                    this.messageStore = <Array<IMessage>>messages;
                    this.messageSubject.next(this.messageStore);
                },
                error => {
                    this.helper.handleMessageError(error, 'get');
                });
    }

    postMessage(message: IMessage) {

        this.http.post<IWrapperResponse>(`${BASE_URL}/messages/post`, message)
            .subscribe(
                response => {
                    const { messages }: any = response.result;
                    this.messageStore = <Array<IMessage>>messages;
                    this.messageSubject.next(this.messageStore);
                },
                error => {
                    this.helper.handleMessageError(error, 'post');
                });

    }

    getUser(userId) {
        return this.http.get<IWrapperResponse>(`${BASE_URL}/users/${userId}/get`, this.auth.tokenHeader).subscribe(
            response => {
                console.log(response);
            },
            error => {
                this.helper.handleMessageError(error, 'post');
            });
    }
    
    getMeUser() {
        return this.http.get<IWrapperResponse>(`${BASE_URL}/users/me`, this.auth.tokenHeader).subscribe(
            response => {
                console.log(response);
            },
            error => {
                this.helper.handleMessageError(error, 'post');
            });
    }
}