
import { MatSnackBar } from '@angular/material/snack-bar';
class Helper {

    constructor(private snackBar: MatSnackBar) {

    }

    handleMessageError(...errs: any[]) {
        console.log('ERROR : ', errs[0]);
        this.snackBar.open(`unable to ${errs[1]} message`, 'close', { duration: 3000 });
    }

    handleTokenError(...errs: any[]) {
        console.log('ERROR : ', errs[0]);
        this.snackBar.open(`unable to get ${errs[1]}`, 'close', { duration: 3000 });
    }

    handleRegisterError(...errs: any[]) {
        console.log('ERROR : ', errs[0]);
        this.snackBar.open(`unable to ${errs[1]} user`, 'close', { duration: 3000 });
    }
}
export {
    Helper
}



