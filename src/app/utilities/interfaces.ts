interface IMessage {
    userId: number;
    userName: string;
    text: string;
    timestamp?: string;
}

interface IWrapperResponse {
    message: string;
    httpStatusCode: string;
    result: Array<any> | Object | string;
    source: string;
    statusCode: number;
}

interface IUser {
    firstName: string,
    surName: string,
    email: string,
    password: string,
    age?: number,
    confirmPassword?: string
}

interface ILoginData {
    email: string,
    password: string
}

export {
    IMessage,
    IWrapperResponse,
    IUser,
    ILoginData
}