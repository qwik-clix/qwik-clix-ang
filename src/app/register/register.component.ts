import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../web.services/auth.service'

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder, private auth: AuthService) {
    this.form = formBuilder.group({
      firstName: ['', Validators.required],
      surName: ['', Validators.required],
      email: ['', [Validators.required, emailValid()]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, { validator: matchingFields('password', 'confirmPassword') });
  }

  ngOnInit(): void {
  }
  onSubmit(): void {
    this.auth.register(this.form.value);
  }

  isValid(control: string | number): boolean {
    return this.form.controls[control].invalid && this.form.controls[control].touched;
  }

}

function matchingFields(f1: string | number, f2: string | number) {
  return (form: { controls: { [x: string]: { value: any; }; }; }) => {
    if (form.controls[f1].value !== form.controls[f2].value) {
      return { mismatchFields: true };
    }
  }
}

function emailValid() {
  return (control: { value: string; }) => {
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(control.value) ? null : { invalidEmail: true };
  }
}

