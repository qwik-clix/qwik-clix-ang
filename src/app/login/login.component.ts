import { Component, OnInit } from '@angular/core';
import { AuthService } from './../web.services/auth.service';
import { ILoginData } from '../utilities/interfaces';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit(): void {
  }

  loginData: ILoginData = { email: '', password: '' };

  login() {
    this.auth.login(this.loginData);
  }
}
